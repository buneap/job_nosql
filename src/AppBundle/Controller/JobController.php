<?php

namespace AppBundle\Controller;

use AppBundle\Document\Adresse;
use AppBundle\Document\Tags;
use AppBundle\Document\Annonce;
use AppBundle\Document\Avis;
use AppBundle\Document\Coordonnees;
use AppBundle\Document\Entreprise;
use AppBundle\Document\TypeContrat;
use Documents\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JobController extends Controller
{
    public function statistiquesAction()
    {

        $tags = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Tags')
            ->findAll();
        $size = sizeof($tags);

        $result = array();

        foreach ($tags as $tag) {
            $result[$tag->getNom()] = 0;
        }

        for ($i = 0; $i < $size; $i++) {

            $annonce = $this->get('doctrine_mongodb')
                ->getRepository('AppBundle:Annonce')
                ->findByTags($tags[$i]);

            $nb = sizeOf($annonce);
            $result[$tags[$i]->getNom()] = $nb;

        }
        asort($result);


        return $this->render("@App/job/stat.html.twig", array(
            "results" => $result
        ));
    }

    public function docAction()
    {


        return $this->render("@App/job/index.html.twig", array());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * Pour recuperer toutes les annonces
     */
    public function AnnoncesAction(Request $request)
    {
        $annonces = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Annonce')
            ->findAll();

        return new JsonResponse($annonces);
    }

    /**
     * @param $id
     * @return JsonResponse
     * pour recuperer une annonce via son id
     */
    public function AnnonceAction($id)
    {
        $annonce = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Annonce')
            ->findOneById($id);

        return new JsonResponse($annonce);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * pour crée une annonce il faut post un json avec les information requise ex :
     *
     * {
     * "intitule" : "Recherche dev",
     * "contenu" : "js php et html",
     * "tags" : ["dev", "javascript" , "web"],
     * "entreprise" : "Agena 3000",
     * "typeContrat" : "CDI"
     *
     * }
     *
     */
    public function postAnnonceAction(Request $request)
    {
        if ($request->getMethod() == "POST") {
            $content = $request->getContent();

            if (!empty($content)) {

                $params = json_decode($content, true); // 2nd param to get as array
                $annonce = $this->createAnnonce($params);

                return new JsonResponse($annonce);
            } else {
                return new JsonResponse("Erreur l'annonce n'a pas été crée");
            }

        } else {
            return new JsonResponse("POST Only");
        }

    }

    public function UpdateAction(Request $request)
    {
        $dm = $this->get('doctrine_mongodb')
            ->getManager();


        $content = $request->getContent();
        if ($content) {
            $params = json_decode($content, true); // 2nd param to get as array
            $annonce = $dm->getRepository('AppBundle:Annonce')
                ->findOneById($params["id"]);
            if ($annonce !== null) {
                $annonce->setIntitule($params["intitule"]);
            }
        }
        $dm->flush();
        return new Response('annonce updated');
    }

    public function DeleteAction($id)
    {
        $dm = $this->get('doctrine_mongodb')
            ->getManager();
        $annonce = $dm->getRepository('AppBundle:Annonce')
            ->findOneById($id);
        $dm->remove($annonce);
        $dm->flush();

        return new Response('annonce deleted');
    }


    public function searchAnnonceAction(Request $request)
    {
        $x = $request->get('x');
        $y = $request->get('y');


        $entreprise = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Entreprise')
            ->findOneByCoordonnees(new Coordonnees($x, $y));
        $annonces = $entreprise->getAnnonces();

        $test = array();

        foreach ($annonces as $annonce) {
            array_push($test, $annonce);
        }
        return new JsonResponse($test);
    }

    /**
     * @param $content
     * @return Annonce
     * pour crée une annonce via un tableau de valeurs
     */
    private function createAnnonce($content)
    {

        $dm = $this->get('doctrine_mongodb')->getManager();

        // on crée une nouvelle annonce
        $annonce = new Annonce();
        $annonce->setDatePublication(new \DateTime());
        $annonce->setIntitule($content["intitule"]);
        $annonce->setContenu($content["contenu"]);

        // on essaye de recuperer la societe via le nom passer dans le tableau
        $entreprise = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Entreprise')
            ->findOneByNom($content["entreprise"]);

        // si on la trouve on la set sur l'annonce
        if ($entreprise !== null) {
            $annonce->setEntreprise($entreprise);

            $entreprise->addAnnonces($annonce);


        }

        // on rechercher de la meme maniere de type de contrat
        $typeContrat = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:TypeContrat')
            ->findOneByNom($content["typeContrat"]);

        // si on le trouve on l'ajoute a l'annonce
        if ($typeContrat !== null) {
            $annonce->setTypeContrat($typeContrat);
        }


        // pour tout les tags on verfie si ils existent sinon on les crée
        foreach ($content["tags"] as $tagName) {

            $tag = $this->get('doctrine_mongodb')
                ->getRepository('AppBundle:Tags')
                ->findOneByNom($tagName);

            if ($tag !== null) {
                $annonce->addTags($tag);

            } else {

                $tag = new Tags();
                $tag->setNom($tagName);
                $annonce->addTags($tag);

                $dm->persist($tag);
            }

        }


        $dm->persist($annonce);
        $dm->flush();

        return $annonce;


    }

    /**
     * pour crée une entreprise avec des informaions remplie
     */
    private function createEntreprise()
    {
        $entreprise = new Entreprise();
        $entreprise->setNom("Agena 3000");
        $entreprise->setSiret("12345678910121");

        $adresse = new Adresse();
        $adresse->setCodePostal("49300");
        $adresse->setRue("rue du paradis");
        $adresse->setVille("Cholet");

        $entreprise->setAdressePostale($adresse);


        $coordonnees = new Coordonnees();
        $coordonnees->setX(10.2);
        $coordonnees->setY(14.5);

        $entreprise->setCoordonnees($coordonnees);


        $avis = new Avis();
        $avis->setDate(new \DateTime());
        $avis->setNote(4);
        $avis->setTitre("satisfait");
        $avis->setCommentaire("entreprise moderne et active");
        $avis->setPointPositif("moderne");
        $avis->setPointNegatif("trop petit");
        $avis->setEntreprise($entreprise);

        $avis2 = new Avis();
        $avis2->setDate(new \DateTime());
        $avis2->setNote(4);
        $avis2->setTitre("satisfait");
        $avis2->setCommentaire("entreprise moderne et active");
        $avis2->setPointPositif("moderne");
        $avis2->setPointNegatif("trop petit");
        $avis2->setEntreprise($entreprise);

        $entreprise->addAvis($avis);
        $entreprise->addAvis($avis2);

        $annonce = new Annonce();

        $annonce->setIntitule("candidature spontanée");
        $annonce->setContenu("nous recherchons des devs");
        $annonce->setDatePublication(new \DateTime());
        $annonce->setTypeContrat(
            $typeContrat = $this->get('doctrine_mongodb')
                ->getRepository('AppBundle:TypeContrat')
                ->findOneByNom("CDI")
        );
        $annonce->setEntreprise($entreprise);
        $entreprise->addAnnonces($annonce);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($avis);
        $dm->persist($avis2);
        $dm->persist($annonce);
        $dm->persist($entreprise);
        $dm->persist($adresse);
        $dm->persist($coordonnees);
        $dm->flush();
    }

    /**
     * @param $titre
     * pour crée un tag
     */
    private function createTag($titre)
    {
        $tag = new Tags();
        $tag->setNom($titre);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($tag);
        $dm->flush();
    }


}
