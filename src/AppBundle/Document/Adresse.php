<?php
/**
 * Created by PhpStorm.
 * User: tetin
 * Date: 31/01/2018
 * Time: 16:43
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 * */
class Adresse implements \JsonSerializable
{
    /**
     * @MongoDB\Field(type="string")
     */
    private $rue;

    /**
     * @MongoDB\Field(type="int")
     */
    private $codePostal;

    /**
     * @MongoDB\Field(type="string")
     */
    private $ville;

    public function jsonSerialize()
    {
        return [
            "rue" => $this->rue,
            "codePostal" => $this->codePostal,
            "ville" => $this->ville,
        ];
    }

    /**
     * @return mixed
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * @param mixed $rue
     */
    public function setRue($rue)
    {
        $this->rue = $rue;
    }

    /**
     * @return mixed
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }


}