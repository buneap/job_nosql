<?php
/**
 * Created by PhpStorm.
 * User: tetin
 * Date: 31/01/2018
 * Time: 13:22
 */

namespace AppBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Entreprise implements \JsonSerializable
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $nom;

    /**
     * @MongoDB\Field(type="string")
     */
    private $siret;

    /**
     * @MongoDB\EmbedOne(targetDocument="Adresse")
     */
    private $adressePostale;

    /**
     * @MongoDB\EmbedOne(targetDocument="Coordonnees")
     */
    private $coordonnees;

    /**
     * @MongoDB\EmbedMany(targetDocument="Annonce")
     */
    private $annonces;

    /**
     * @MongoDB\EmbedMany(targetDocument="Avis")
     */
    private $avis;

    public function __construct()
    {
        $this->avis = new ArrayCollection();
        $this->annonces = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "nom" => $this->nom,
            "siret" => $this->siret,
            "adresse" => $this->adressePostale,
            "gps" => $this->coordonnees,
            "annonces" => $this->annonces,
            "avis" => $this->avis
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param mixed $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return mixed
     */
    public function getAdressePostale()
    {
        return $this->adressePostale;
    }

    /**
     * @param mixed $adressePostale
     */
    public function setAdressePostale($adressePostale)
    {
        $this->adressePostale = $adressePostale;
    }

    /**
     * @return mixed
     */
    public function getCoordonnees()
    {
        return $this->coordonnees;
    }

    /**
     * @param mixed $coordonnees
     */
    public function setCoordonnees($coordonnees)
    {
        $this->coordonnees = $coordonnees;
    }

    /**
     * @return mixed
     */
    public function getAnnonces()
    {
        return $this->annonces;
    }

    /**
     * @param mixed $annonces
     */
    public function addAnnonces($annonces)
    {

        $this->annonces[] = $annonces;
    }


    /**
     * @return mixed
     */
    public function getAvis()
    {
        return $this->avis;
    }

    /**
     * @param mixed $avis
     */
    public function addAvis($avis)
    {
        $this->avis[] = $avis;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


}

