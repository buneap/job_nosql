<?php
/**
 * Created by PhpStorm.
 * User: tetin
 * Date: 31/01/2018
 * Time: 16:12
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Avis implements \JsonSerializable
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Entreprise", inversedBy="avis")
     */
    private $entreprise;

    /**
     * @MongoDB\Field(type="int")
     */
    private $note;
    /**
     * @MongoDB\Field(type="string")
     */
    private $titre;
    /**
     * @MongoDB\Field(type="string")
     */
    private $commentaire;
    /**
     * @MongoDB\Field(type="string")
     */
    private $pointPositif;
    /**
     * @MongoDB\Field(type="string")
     */
    private $pointNegatif;
    /**
     * @MongoDB\Field(type="date")
     */
    private $date;

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "note" => $this->note,
            "titre" => $this->titre,
            "commentaire" => $this->commentaire,
            "pointPositif" => $this->pointPositif,
            "pointNegatif" => $this->pointNegatif,
            "date" => $this->date
        ];
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param mixed $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getPointPositif()
    {
        return $this->pointPositif;
    }

    /**
     * @param mixed $pointPositif
     */
    public function setPointPositif($pointPositif)
    {
        $this->pointPositif = $pointPositif;
    }

    /**
     * @return mixed
     */
    public function getPointNegatif()
    {
        return $this->pointNegatif;
    }

    /**
     * @param mixed $pointNegatif
     */
    public function setPointNegatif($pointNegatif)
    {
        $this->pointNegatif = $pointNegatif;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}