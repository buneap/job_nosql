<?php

namespace AppBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document
 */
class Annonce implements \JsonSerializable
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $intitule;

    /**
     * @MongoDB\Field(type="date")
     */
    private $datePublication;

    /**
     * @MongoDB\Field(type="string")
     */
    private $contenu;

    /**
     * @var
     * @MongoDB\EmbedMany(targetDocument="Tags")
     */
    private $tags;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Entreprise", inversedBy="annonces")
     */
    private $entreprise;

    /**
     * @MongoDB\ReferenceOne(targetDocument="TypeContrat", inversedBy="annonces")
     */
    private $typeContrat;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "intitule" => $this->intitule,
            "datePublication" => $this->datePublication,
            "contenu" => $this->contenu,
            "tags" => $this->tags,
            "entreprise" => $this->entreprise,
            "typeContrat" => $this->typeContrat
        ];
    }


    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return mixed
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $datePublication
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function addTags($tags)
    {
        $this->tags[] = $tags;
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param mixed $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return mixed
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * @param mixed $typeContrat
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;
    }


}
