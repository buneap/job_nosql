<?php
/**
 * Created by PhpStorm.
 * User: tetin
 * Date: 31/01/2018
 * Time: 16:44
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 * */
class Coordonnees implements \JsonSerializable
{
    /**
     * @MongoDB\Field(type="float")
     */
    private $x;

    /**
     * @MongoDB\Field(type="float")
     */
    private $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function jsonSerialize()
    {
        return [
            "x" => $this->x,
            "y" => $this->y,
        ];
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }


}
