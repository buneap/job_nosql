<?php
/**
 * Created by PhpStorm.
 * User: tetin
 * Date: 31/01/2018
 * Time: 13:22
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class TypeContrat implements \JsonSerializable
{


    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $nom;
    /**
     * @MongoDB\ReferenceMany(targetDocument="Annonce", inversedBy="typeContrat")
     */
    private $annonces;


    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "nom" => $this->nom,
            "annonces" => $this->annonces
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getAnnonces()
    {
        return $this->annonces;
    }

    /**
     * @param mixed $annonces
     */
    public function setAnnonces($annonces)
    {
        $this->annonces = $annonces;
    }

}